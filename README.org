* Torrents-reblocks

Search for torrents.

Interactive Weblocks interface. It's working \o/

http://40ants.com/weblocks/quickstart.html


** Usage

#+BEGIN_SRC lisp
   (ql:quickload "torrents-reblocks")
   (torrents-reblocks:start)
#+END_SRC

#+html: <p align='center'><img src='img.png' /></p>

#+html: <p align='center'><img src='img-magnet.png' /></p>

** Installation

   Dependencies to clone in local-projects (see =make install=):

: https://github.com/vindarel/cl-torrents

: https://github.com/40ants/weblocks

: https://github.com/40ants/weblocks-ui


   To build an executable:

: make build

   run it:

: ./torrents-web
#+BEGIN_SRC text
 <INFO> [16:35:53] weblocks/server server.lisp (start) -
  Starting weblocks WEBLOCKS/SERVER::PORT: 40000
  WEBLOCKS/SERVER::SERVER-TYPE: :HUNCHENTOOT DEBUG: T
#+END_SRC

And access it on http://localhost:40000/torrents (warn the =torrents= suffix).

https://lisp-journey.gitlab.io/web-dev/#shipping

Debian dependencies:

- =libreadline-dev=, =libssl-dev=, =libssl1.0.2=.


** Development notes

- after compiling a function and a  page refresh, we keep the state of
  the app. Only a =reset= looses it.

** Troubleshooting

#+BEGIN_SRC text
Don't know how to REQUIRE sb-cltl2.
#+END_SRC

Set =SBCL_HOME= to the output of =(sb-int:sbcl-homedir-pathname)=.

LICENCE: WTF public licence.
